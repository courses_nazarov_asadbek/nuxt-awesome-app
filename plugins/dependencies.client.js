import Vue from 'vue'
import VueTheMask from 'vue-the-mask'

export default ({ app, store }) => {
  Vue.use(VueTheMask)
}
