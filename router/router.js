import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
// const guard = function (to, from, next) {
//   // check for valid auth token
//   if (getToken()) {
//     next();
//   } else {
//     window.location.href = "/login"
//   }
// };

export function createRouter() {
  const router = new Router({
    mode: 'history',
    routes: [
      {
        path: '',
        component: () => import('@/pages/index').then(m => m.default || m),
        children: [
          {
            path: '',
            // beforeEnter: (to, from, next) => {guard(to, from, next);},
            redirect: {name: 'dashboard'}
          },
          {
            path: 'dashboard',
            // beforeEnter: (to, from, next) => {guard(to, from, next);},
            component: () => import('@/pages/dashboard/layout').then(m => m.default || m),
            children: [
              {
                path: '', name: 'dashboard',
                component: () => import('@/pages/dashboard/index').then(m => m.default || m)
              },
            ]
          },
          {
            path: 'inspire',
            component: () => import('@/pages/inspire/layout').then(m => m.default || m),
            children: [
              {
                path: '', name: 'inspire',
                component: () => import('@/pages/inspire/index').then(m => m.default || m),
              }
            ]
          }
        ],
      },
      // {
      //   path: '/login',
      //   name: 'login',
      //   component: () =>
      //     import('@/pages/auth/login').then(m => m.default || m),
      // },
      {
        path: '/error',
        name: 'error_layout',
        component: () =>
          import('@/layouts/error').then(m => m.default || m),
      },
      {path: '*', redirect: {name: 'error_layout'}},
    ],
  })

  router.beforeEach((to, from, next) => next())

  return router
}
