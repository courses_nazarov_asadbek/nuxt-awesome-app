import ru from 'vuetify/es5/locale/ru'
let current_date = new Date().getHours()

export default function () {
  return {
    font: false,
    treeShake: true,
    theme: {
      dark: current_date >= 20 || current_date < 4 ? true : false,
      themes: {
        light: {
          primary: '#388087',
          secondary: '#6FB3B8',
          tertiary: '#BADFE7',
          quaternary: '#C2EDCE',
          quinary: '#F6F6F2',
          senary: '#ffffff',
          septenary: '#ffffff',
          octonary: '#EDEDED',
          nonary: '#FEFCED',
          denary: '#eff7fd',
          accent: '#FAEEFA',
          error: '#FF5252',
          info: '#2196F3',
          success: '#4CAF50',
          warning: '#FFC107',
          basic: '#ffffff',
          background: '#F8F8F8'
        },
        dark: {
          primary: '#7367f0',
          secondary: '#283046',
          tertiary: '#43455C',
          quaternary: '#3C3F58',
          quinary: '#707793',
          senary: '#161d31',
          septenary: '#FFFFFF',
          octonary: '#FFFFFF',
          nonary: '#FFFFFF',
          denary: '#FFFFFF',
          accent: '#FFFFFF',
          error: '#FF5252',
          info: '#2196F3',
          success: '#4CAF50',
          warning: '#FFC107',
          basic: '#283046',
          background: '#161d31',
        }
      }
    },
    options: {
      customProperties: true,
      themeCache: {
        get: (key) => localStorage.getItem(key),
        set: (key, value) => localStorage.setItem(key, value),
      }
    },
    lang: {
      locales: { ru },
      current: 'ru'
    }
  }
}
