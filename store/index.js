export const state = () => ({
  messages: {
    title: '',
    options: {}
  },
  defaultLanguage: 'RU',
  languages: [],
  user: {},
  menu: {},
  leftMenuButton: {
    drawer: true,
    clipped: false,
    miniVariant: false,
    dense: true
  },
  rightMenuButton: {
    rightDrawer: false,
    navTemporary: true,
    list: []
  },
})

export const mutations = {
  setMessages(state, messages) {
    state.messages = {
      title: messages.title,
      options: messages.options
    }
  },
  setChangeRightMenu(state, condition = {
    rightDrawer: false,
    navTemporary: true
  }) {
    state.rightMenuButton = {
      rightDrawer: condition?.rightDrawer,
      navTemporary: condition?.navTemporary,
      list: condition?.list
    }
  },
  setChangeLeftMenu(state, type) {
    console.log('mut', type)
    state.leftMenuButton[type] = !state.leftMenuButton[type]
  }
}

export const actions = {
  getMutateRightMenu({commit, state}, payload) {
    if (payload?.rightDrawer === undefined) {
      payload = {
        rightDrawer: !state.rightMenuButton.rightDrawer,
        navTemporary: payload?.navTemporary !== undefined ? payload?.navTemporary : true,
        list: payload?.list !== undefined ? payload?.list !== undefined : []
      }
    }
    commit('setChangeRightMenu', payload)
  },
}
export const getters = {
  GET_RIGHT_MENU: state => state.rightMenuButton,
  GET_LEFT_MENU: state => state.leftMenuButton,
  GET_MESSAGES: state => state.messages
}
