import appConfig from "./config/main"

export default {
  ssr: appConfig.app_mode,
  target: 'server',
  server: {
    port: appConfig.app_port
  },
  head: {
    titleTemplate: '%s - nuxt-awesome-app',
    title: 'nuxt-awesome-app',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      {charset: 'utf-8'},
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: ''
      },
      {name: 'format-detection', content: 'telephone=no'}
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ]
  },

  css: [
    '@/assets/styles/main.scss'
  ],

  plugins: [
    {src: '@plugins/i18n'},
    {src: '@plugins/dependencies.client'},
  ],

  components: true,

  buildModules: [
    '@nuxtjs/vuetify',
    ['@nuxtjs/router', {
      path: './router'
    }]
  ],

  modules: [
    '@nuxtjs/axios',
    'nuxt-i18n',
    ['vue-toastification/nuxt', {
      position: 'top-right',
      timeout: 2500,
      closeOnClick: false,
      draggable: false,
      closeButton: false,
      icon: true,
      transition: "Vue-Toastification__fade",
      maxToasts: 3,
      newestOnTop: false,
      hideProgressBar: true
    }]
  ],

  axios: {},
  i18n: {
    strategy: 'no_prefix',
    defaultLocale: appConfig.app_language,
    langDir: 'i18n/',
    lazy: true,
    locales: appConfig.app_language_list,
    detectBrowserLanguage: {
      useCookie: true,
      alwaysRedirect: false,
      cookieKey: 'user_choose_language'
    },
    vueI18n: {
      silentTranslationWarn: true,
      silentFallbackWarn: true,
    }
  },
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    // defaultAssets: false,
    optionsPath: './vuetify.options.js',
    treeShake: true
  },
  router: {
    base: appConfig.base_route,
    linkActiveClass: 'active',
    linkExactActiveClass: 'exact-active',
    //middleware: ['auth']
  },


  build: {}
}
